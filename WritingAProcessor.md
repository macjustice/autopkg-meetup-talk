theme: Letters from Sweden, 4
# Writing an Autopkg Processor
## [fit] Mac Justice - Client Platform Engineer, Dropbox

---
# Hack Week
- Munki + Git
- Why not just upload the packages directly?

^ At Dropbox, we hold a hack week twice a year. It's a week where we put our usual workload on hold to try something new, learn a new skill, or build that thing you otherwise never have time for.

^ I wanted to try a different approach to how we manage Munki in git, by excluding the packages from version control and simply uploading them to the web host as we get them.

---
# Autopkg 101

From the Readme:
>AutoPkg is an automation __framework__ for macOS software packaging and distribution, oriented towards the tasks one would normally perform manually to prepare third-party software for mass deployment to managed clients.

^ Emphasis on the framework. Autopkg itself doesn't do the work, it defines a way of writing and using modular actions, which are called processors. 

^ An Autopkg recipe is a file that contains a series of steps, and each step is a command issued to a processor.  

---
# Autopkg 101
## Typical Recipe Operations:
1. Download an application *URLDownloader*
2. Unpack *PkgExtractor, DmgMounter*
3. Manipulate *FileMover, PathDeleter, PlistEditor*
4. Repackage *PkgCreator, DmgCreator*
5. Import to management system *MunkiImporter, JSSImporter, etc*

^ Overview of a example recipe, with processors you might use for each step

---
# S3Uploader

![fit right](https://miro.medium.com/max/480/1*1A1CQ8a-vKphpDu97_U6Kw.png)

A processor to upload packages to Amazon Web Services S3 

^ I'm interested in moving our Munki hosting to AWS, so I thought maybe I'd try my hand at writing a processor to upload packages to S3 in an AutoPkg recipe. If you're unfamiliar with S3, it's an object storage service, which can be used like a web server for static files. It's inexpensive, roughly .02/GB/Month for storage and .09/GB for your first 10TB of downloads, and EXTREMELY reliable.

---
# Writing a Processor 
## How to get started
- Python
- Git + GitHub 
- Work from an example (SampleSharedProcessor)
- Look at other processors
- Go join the #autopkg channel 

^ So I set out to write my first AutoPkg processor. To start you'll need some basic familiarity with Python and git, and probably a free GitHub account.

^ There's a processor in the main autopkg recipe repo called "SampleSharedProcessor" that's a template to work from.

^ Since the first party and many third party AutoPkg processors are open source, I tried reading through a few on GitHub, to get a sense of how they work and what better coders than me do.  

---
## NumberDoubler.py
### Extremely Minimal Processor Example
```python
from autopkglib import Processor

class NumberDoubler(Processor):
    input_variables = { "double_me": {"description": "A number to double"}}
    output_variables = { "doubled:" {"description": "double_me times two"}}

    def main(self):
        self.env['doubled'] = self.env['double_me'] * 2

if __name__ == "__main__":
    PROCESSOR = NumberDoubler()
    PROCESSOR.execute_shell()
```

^ A stripped back example of a processor

^ Import the Processor and ProcessorError classes from autopkglib

^ Make a new class MyProcessor, based on Processor

^ Define input and output variables. They're dictionaries, follow the ones you'll see in example processors.

---
## NumberDoubler.py
### Extremely Minimal Processor Example
```python
from autopkglib import Processor

class NumberDoubler(Processor):
    input_variables = { "double_me": {"description": "A number to double"}}
    output_variables = { "doubled:" {"description": "double_me times two"}}

    def main(self):
        self.env['doubled'] = self.env['double_me'] * 2

if __name__ == "__main__":
    PROCESSOR = NumberDoubler()
    PROCESSOR.execute_shell()
```

^ Make a main method, which is called when your processor is called. Your input and output variables are accessed as class environment variables. Here I take the "double me" variable, multiply it by 2, and store the result in the "doubled" variable, which autopkg can pass on to another processor

^ The last `if` statement is called when Autopkg calls the processor, you can look at the execute_shell() method of the Processor class for more info.

---
# [fit] S3Uploader, today

^ So, what about S3Uploader?

^ I got it to upload a file to an S3 bucket. But by that point I wasn't sure if this was the approach I liked to hosting Munki in S3. And so.

---

![fit original](https://bukk.it/picard-done.gif)


---
# Hurdles
- Lack of documentation for writing a processor
- Understanding the execution context (Where does this run? With what?)
  - Just running the script from ~/src/macjustice-recipes/S3uploader/ :no_good:
  - How'm I supposed to develop this thing?


^ While there's an example processor, there isn't much info other than that for how to write a processor. Fortunately Autopkg is open source, so you can try to answer questions by reading the code.

^ I had a really hard time wrapping my head around the execution context, and so while I could write a simple script to upload a file, putting it into an Autopkg processor was confusing for me

---
# Final Thoughts

- Open Source documentation is a challenge
- It's a jump from simple scripting to working with frameworks
- Lots of respect for the Autopkg maintainers and community

---
# Other Info and Link to This Talk
- [AutoPkg Wiki](https://github.com/autopkg/autopkg/wiki)
- [SampleSharedProcessor](https://github.com/autopkg/recipes/tree/master/SampleSharedProcessor)
- Penn State MacAdmins
  - [Greg Neagle - You Oughta Check Out AutoPkg](https://www.youtube.com/watch?v=mqK-MAEZekI&t=1250s)
  - [Anthony Reimer - AutoPkg Level-Up](https://www.youtube.com/watch?v=BI10WWrgG2A)
  - [Elliot Jordan - How (Not) To Do Bad Things With AutoPkg](https://www.youtube.com/watch?v=LnvQmLcDF8w)


![right](qr.png)

<!--
Notes stuff

All processors are written in Python
Work from an example processor https://github.com/autopkg/recipes/tree/master/SampleSharedProcessor

`defaults read com.github.autopkg RECIPE_SEARCH_DIRS`
search path order matters!

Recipes are a chain: Parent -> child

SECURITY: See [Elliot's talk](https://www.youtube.com/watch?v=LnvQmLcDF8w). Recipes are ultimately a form of arbitrary execution, so go look at what you're actually doing.  

```python
class MyProcessor(Processor):
    input_variables = {}
    output_variables = {}

```

-->


